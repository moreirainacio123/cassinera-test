<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateUploadFilesTable.
 */
class CreateUploadFilesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('upload_files', function(Blueprint $table) {
            $table->increments('id');
            $table->string('file')->nullable();
            $table->string('title')->nullable();
            $table->enum('type', ['IMAGEM', 'PDF'])->nullable();
            $table->timestamps();
            $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('upload_files');
	}
}
