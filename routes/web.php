<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/register', function () {
    return view('auth.register');
})->name('register');

Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    Route::prefix('dashboard')->group(function(){
        Route::get('/', function(){
            return view('dashboard');
        })->name('dashboard');
    });

    Route::prefix('arquivos')->group(function(){
        Route::get('/', 'App\Http\Controllers\UploadFilesController@index')->name('arquivos');
        Route::get('/create', 'App\Http\Controllers\UploadFilesController@create')->name('arquivos-create');
        Route::post('/store', 'App\Http\Controllers\UploadFilesController@store')->name('arquivos-store');
        Route::get('/{id}/edit', 'App\Http\Controllers\UploadFilesController@edit')->name('arquivos-edit');
        Route::post('/{id}/update', 'App\Http\Controllers\UploadFilesController@update')->name('arquivos-update');
        Route::get('/list-files', 'App\Http\Controllers\UploadFilesController@listFiles')->name('list-files');
        Route::get('/{id}/destroy', 'App\Http\Controllers\UploadFilesController@destroy')->name('arquivos-destroy');
        Route::get('/{id}/show', 'App\Http\Controllers\UploadFilesController@getUploadedFile')->name('arquivos-show');
    });
});