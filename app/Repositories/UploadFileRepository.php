<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UploadFileRepository.
 *
 * @package namespace App\Repositories;
 */
interface UploadFileRepository extends RepositoryInterface
{
    //
}
