<?php


namespace App\Services;


class UploadFileService
{
    public function run($request)
    {
        $fileNameToStore = '';

        if($request->file('uploadFile')){
            //NOME ORIGINAL DO ARQUIVO
            $filenameWithExt = $request->file('uploadFile')->getClientOriginalName();
            //NOME DO ARQUIVO SEM EXTENSAO
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //PEGA A EXTENSAO DO ARQUIVO
            $extension = $request->file('uploadFile')->getClientOriginalExtension();
            //ARUIVO A SER GRAVADO
            $fileNameToStore = $filename . '_' . date('Y-m-d-H-i-s') . '.' . $extension;
            //ENVIA O ARQUIVO PARA UPLOAD
            $request->file('uploadFile')->storeAs('public/files', $fileNameToStore);
        }else{
            $fileNameToStore = '';
        }


        return $fileNameToStore;
    }
}