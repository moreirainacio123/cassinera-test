<?php


namespace App\Services;

use App\Models\UploadFile;

class UpdateUploadFileService
{
    protected $uploadFile;

    public function __construct(UploadFile $uploadFile)
    {
        $this->uploadFile = $uploadFile;
    }

    /**
     * INSERE OS REGISTROS NO BANCO
     */
    public function run(Array $array)
    {
        try {
            $store = $this->uploadFile->find($array['id']);
            return $store->update($array);
        } catch (\Exception $e) {
            return [
                'errors' => [
                    'title' => $e->getMessage(),
                    'file' => $e->getFile()
                ]
            ];
        }
    }
}