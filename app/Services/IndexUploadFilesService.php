<?php


namespace App\Services;

use App\Models\UploadFile;

class IndexUploadFilesService
{
    protected $uploadFile;

    public function __construct(UploadFile $uploadFile)
    {
        $this->uploadFile = $uploadFile;
    }

    /**
     * TRAZ A LISTAGEM DE ARQUIVOS GRAVADOS NO BANCO
     */
    public function run()
    {
        try {
            return $this->uploadFile->all();
        } catch (\Exception $e) {
            return [
                'errors' => [
                    'title' => $e->getMessage(),
                    'file' => $e->getFile()
                ]
            ];
        }
    }
}