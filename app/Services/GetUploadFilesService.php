<?php


namespace App\Services;

use App\Models\UploadFile;

class GetUploadFilesService
{
    protected $uploadFile;

    public function __construct(UploadFile $uploadFile)
    {
        $this->uploadFile = $uploadFile;
    }

    /**
     * TRAZ A LISTAGEM DE ARQUIVOS GRAVADOS NO BANCO
     */
    public function run($id)
    {
        try {
            return $this->uploadFile->find($id);
        } catch (\Exception $e) {
            return [
                'errors' => [
                    'title' => $e->getMessage(),
                    'file' => $e->getFile()
                ]
            ];
        }
    }
}