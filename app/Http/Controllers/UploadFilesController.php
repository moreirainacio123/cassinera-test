<?php

namespace App\Http\Controllers;

use App\Http\Requests\UploadFileCreateRequest;
use App\Http\Requests\UploadFileUpdateRequest;
use App\Models\UploadFile;
use App\Services\GetUploadFilesService;
use App\Services\IndexUploadFilesService;
use App\Services\StoreUploadFileService;
use App\Services\UpdateUploadFileService;
use App\Services\UploadFileService;
use App\Validators\UploadFileValidator;

/**
 * Class UploadFilesController.
 *
 * @package namespace App\Http\Controllers;
 */
class UploadFilesController extends Controller
{

    private $title = 'Arquivos';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $data['title'] = $this->title;
        return view('uploadfiles.index', $data);
    }


    /**
     * CHAMADO VIA AJAX
     */
    public function listFiles(IndexUploadFilesService $indexUploadFilesService)
    {
        $response = $indexUploadFilesService->run();
        return $response;
    }

    public function create()
    {
        $data['title'] = $this->title;
        return view('uploadfiles.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UploadFileCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(
        UploadFileCreateRequest $request,
        UploadFileService $uploadFileService,
        StoreUploadFileService $storeUploadFileService
    )
    {
        $file = $uploadFileService->run($request);
        $params = $request->all();
        $params['file'] = $file;
        $arrayType = explode('.', $file);
        $params['type'] = strtolower(end($arrayType)) == 'pdf' ? 'PDF' : 'IMAGEM';

        return $storeUploadFileService->run($params);

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['title'] = $this->title;
        $data['id'] = $id;
        return view('uploadFiles.edit', $data);
    }

    /**
     * CHAMADO VIA AJAX
     */
    public function getUploadedFile(GetUploadFilesService $getUploadFilesService, $id)
    {
        return $getUploadFilesService->run($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UploadFileUpdateRequest $request
     * @param string $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(
        UploadFileUpdateRequest $request,
        UploadFileService $uploadFileService,
        UpdateUploadFileService $updateUploadFileService,
        $id
    )
    {
        $file = $uploadFileService->run($request);
        $params = $request->all();
        $params['id'] = $id;

        if ($file) {
            $params['file'] = $file;
            $arrayType = explode('.', $file);
            $params['type'] = strtolower(end($arrayType)) == 'pdf' ? 'PDF' : 'IMAGEM';
        }

        return $updateUploadFileService->run($params);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $upload = UploadFile::find($id);
        $upload->delete();
    }
}
